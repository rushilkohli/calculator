package sheridan;
public class CalculateDemo {
public static void main(String[] args) {
    Add add = new Add(4, 6);
    Subtract sub = new Subtract(10,5);
    System.out.println("Addition result: " + add.calculate());
    System.out.println("Subtraction result: " +sub.calculate());
}
}
