package sheridan;
public class Add extends Calculator{
public Add(double operand1, double operand2) {
super(operand1, operand2);
}
@Override
public double calculate() {
return operand1 + operand2;
}
}
